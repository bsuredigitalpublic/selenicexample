package com.learnautomatedtesting.selenic;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class FillinFormPage extends PageObject {
	
	//constructor
	public FillinFormPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	
	//setup the Pageobjects
	
	
    @FindBy(id="exampleInputFirstName")
	private WebElement firstName;
	
	@FindBy(id="exampleInputLastName")
	private WebElement lastName;

	@FindBy(id="exampleInputEmail")
	private WebElement email;

	@FindBy(id="exampleInputPassword")
	private WebElement password;
	
	@FindBy(id="exampleInputPhoneNumber")
	private WebElement phonenumber;
	
	@FindBy(id="btnSubmit")
	private WebElement submit;
	
	//setup logical and properfields based on id/css/xpath for the page elements
	public void FirstName(String firstName){
		this.firstName.clear();
		this.firstName.sendKeys(firstName);

	
	}

	public void LastName(String lastname){
		this.lastName.clear();
		this.lastName.sendKeys(lastname);
		
	
	}
	
	public void email(String email){
		this.email.clear();
		this.email.sendKeys(email);
		
	
	}
	
	public void Password(String password) {
		 this.password.clear();
		 this.password.sendKeys(password);
		
	}
	
	public void PhoneNumber(String phonenumber) {
		 this.phonenumber.clear();
		 this.phonenumber.sendKeys(phonenumber);
		
	}

	public void ClickSubmit() {
		
		this.submit.click();
	}
}
